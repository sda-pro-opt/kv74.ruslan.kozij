#include <tuple>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <experimental/filesystem>
#include <iomanip>

namespace fs = std::experimental::filesystem;
using namespace std;

std::vector<std::tuple<std::string, int>> key_words = {
        make_tuple("BEGIN", 401),
        make_tuple("END", 402),
        make_tuple("PROGRAM", 403),
        make_tuple("CONST", 404),
        make_tuple("$EXP", 405)
};
std::vector<std::tuple<std::string, int, int, int>> buffer;
std::vector<std::string> syntaxBuffer;
std::vector<std::tuple<std::string, int>> identificator;
std::vector<std::tuple<std::string, int>> complex;
bool isError = false;


int search_key_words(string key_word) {
    string lexem;
    int code;
    for(auto const& value: key_words)  {
        std::tie(lexem, code) = value;
        if(lexem == key_word)
            return code;
    }
    return 0;
}

int check_complex_words(string word) {
    string lexem;
    int code;
    for(auto const& value: complex)  {
        std::tie(lexem, code) = value;
        if(!lexem.compare(word)) {
            return code;
        }
    }
    return 0;
}

void set_complex(string word, int row, int line, int &count) {
    int code = check_complex_words(word);
    if(!code) {
        buffer.emplace_back(std::make_tuple(word, count, row, line));
        complex.emplace_back(std::make_tuple(word, count++));
    } else {
        buffer.emplace_back(std::make_tuple(word, code, row, line));
    }
}

int check_identifier_words(string word) {
    string lexem;
    int code;
    for(auto const& value: identificator)  {
        std::tie(lexem, code) = value;
        if(!lexem.compare(word)) {
            return code;
        }
    }
    return 0;
}

void set_identifier(string word, int row, int line, int &count) {
    int code = check_identifier_words(word);
    if(!code) {
        buffer.emplace_back(std::make_tuple(word, count, row, line));
        identificator.emplace_back(std::make_tuple(word, count++));
    } else {
        buffer.emplace_back(std::make_tuple(word, code, row, line));
    }
}

void outputError (string error, ofstream& output, int row, int line) {
    output << error << " (row: " << row << " line: " << line << ")" << endl;
}

void outputResult(ofstream& output) {
    string lexem;
    int ascii, row, line;
    for(auto const& value: buffer)  {
        std::tie(lexem, ascii, row, line) = value;
        output << setw(10) << left << lexem <<
               setw(10) << left << ascii <<
               setw(6)<<" line: " << line <<  setw(5) << left<< " row: "<< row << std::endl;
    }
}

void lexemAnalize (fs::path filePath, fs::path fileOutput) {
    string lexem;
    bool isComment = false,
            isComplex = false,
            isBracket = false,
            isConstant = false;
    int line = 1,
            row = 0,
            row_comment,
            line_comment,
            ascii,
            count_identificator = 1001,
            count_complex = 501;

    const fs::path workdir = fs::current_path();
    const fs::path configPath = workdir.generic_string() + "/tests/" + fileOutput.generic_string() + "/generatedLexem.txt";

    ofstream output(configPath);

    ifstream test(filePath);

    char symbol{0};

    while( test.get(symbol))
    {
        row++;
        ascii = (int)symbol;

        if(!lexem.compare("(*")) {        // equals beginning comments
            row_comment = row - 1;
            line_comment = line;
            isComment = true;
        }

        if(isBracket && ascii == 42) {
            lexem += symbol;
        }
        else if(isBracket && !isComment && lexem == "(") {
            buffer.emplace_back(std::make_tuple(lexem, 40, row - 1, line));
            lexem = "";
        }


        if(isComment && ascii == 42) {
            lexem = symbol;
        }
        else if(isComment && ascii == 41) {
            if(lexem.length() == 1)
                lexem += symbol;
        }

        if(isComment) {
            if(!lexem.compare("*)")) {
                isComment = false;
                isBracket = false;
            }
            if(symbol != '*')
                lexem = "";
            if(symbol == '\n') {
                row = 0;
                line++;
            }

        }
        else if(ascii == 40) { //(
            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(isConstant) {
                set_complex(lexem, row - lexem.length(), line, count_complex);
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            isConstant = false;
            lexem = symbol;
            isBracket = true;
        }
        else if(ascii >= 48 && ascii <= 57) {
            if(lexem.length() == 0) {
                isConstant = true;
            }
            lexem += symbol;
        }
        else if((ascii >= 65 && ascii <= 90) || (ascii >= 97 && ascii <= 122)) { // letters
            if(isConstant) {
                outputError("first symbol digit is incorrect", output, row - lexem.length(), line);

                isError = true;
                break;
            }
            lexem += symbol;
        }
        else if(ascii == 32 || ascii == 9) { // space or tab

            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(isConstant) {
                set_complex(lexem, row - lexem.length(), line, count_complex);
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            isConstant = false;
            lexem = "";
        }
        else if(ascii == 59 || ascii == 46 || ascii == 61 || ascii ==44 || ascii == 39 || ascii == 36 || ascii == 41) {//if symbols equal ; . = , ' $ )
            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(isConstant) {
                set_complex(lexem, row - lexem.length(), line, count_complex);
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            isConstant = false;
            if(ascii == 41) {
                buffer.emplace_back(std::make_tuple(")", ascii, row, line));
                isBracket = false;
            } else if(ascii != 36){
                string temp;
                temp = symbol;
                buffer.emplace_back(std::make_tuple(temp, ascii, row, line));
            }

            lexem = "";

            if(ascii == 36)
                lexem += "$";
        }
        else if(symbol == '\n') {
            isConstant = false;
            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            lexem = "";
            line++;
            row = 0;
        }
        else if(ascii != 39 && ascii != 42){
            outputError("not existing symbol", output, row, line);
            isError = true;
            break;
        }

    }
    if(isComment || isBracket) {
        outputError("comment isn't close", output, row_comment, line_comment);
        isError = true;
    }
    if(!isError)
        outputResult(output);



    output.close();
}



//////////////////////////////////////////////////////////////////////////
////////////////////////LAB 2 SYNTAX ANALIZE//////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


void outputErrorForSyntax (string expected, string founded, ofstream& output, int row, int line) {
    syntaxBuffer.emplace_back("Expected " + expected + " Found " + founded + " (row: " + to_string(row) + " line: " + to_string(line) + ")");
}

void output_syntax_result(ofstream& output) {
    for(auto const& value: syntaxBuffer)  {
        output << value << std::endl;
    }
}

void add_to_syntax_buffer (int depth, string lexem, int ascii, string segment) {
    int temp = 0;
    string syntax_str = "";
    while (temp <= depth) {
        syntax_str += "____";
        temp++;
    }
    syntaxBuffer.emplace_back(syntax_str + segment);
    syntax_str += "____";
    if(ascii == 0)
        syntaxBuffer.emplace_back(syntax_str  + lexem);
    else
        syntaxBuffer.emplace_back(syntax_str + to_string(ascii) + " " + lexem);
}

void add_to_syntax_buffer_segment (int depth, string lexem, int ascii) {
    int temp = 0;
    string syntax_str = "";
    while (temp <= depth) {
        syntax_str += "____";
        temp++;
    }
    if(ascii == 0)
        syntaxBuffer.emplace_back(syntax_str  + lexem);
    else
        syntaxBuffer.emplace_back(syntax_str + to_string(ascii) + " " + lexem);
}

void right_part(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    add_to_syntax_buffer_segment(depthTree, "<right-part>", 0);

    depthTree++;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if (lexem == "'"){
        add_to_syntax_buffer_segment(depthTree, "<empty>", 0);
        depthTree--;
        return;
    }

    if(lexem == ","){
        add_to_syntax_buffer_segment(depthTree, ",", ascii);
        iterator++;

        std::tie(lexem, ascii, row, line) = buffer.at(iterator);
        if(ascii >= 501 && ascii < 1000){
            add_to_syntax_buffer(depthTree, lexem, ascii, "<unsigned-integer>");
            iterator++;
        }
        else {
            isError = true;
            outputErrorForSyntax("<unsigned-integer>", lexem, output, row, line);
            return;
        }
    }
    else if (lexem == "$EXP") {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;

        std::tie(lexem, ascii, row, line) = buffer.at(iterator);
        if(lexem == "("){
            add_to_syntax_buffer_segment(depthTree, lexem, ascii);
            iterator++;
        }
        else {
            isError = true;
            outputErrorForSyntax("(", lexem, output, row, line);
            return;
        }

        std::tie(lexem, ascii, row, line) = buffer.at(iterator);
        if(ascii >= 501 && ascii < 1000){
            add_to_syntax_buffer(depthTree, lexem, ascii, "<unsigned-integer>");
            iterator++;
        }
        else {
            isError = true;
            outputErrorForSyntax("<unsigned-integer>", lexem, output, row, line);
            return;
        }

        std::tie(lexem, ascii, row, line) = buffer.at(iterator);
        if(lexem == ")"){
            add_to_syntax_buffer_segment(depthTree, lexem, ascii);
            iterator++;
        }
        else {
            isError = true;
            outputErrorForSyntax(")", lexem, output, row, line);
            return;
        }
    }
    else {
        isError = true;
        outputErrorForSyntax(",", lexem, output, row, line);
    }

    depthTree--;

}

void left_part(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    add_to_syntax_buffer_segment(depthTree, "<left-part>", 0);

    depthTree++;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(ascii >= 501 && ascii < 1000) {
        add_to_syntax_buffer(depthTree, lexem, ascii, "<unsigned-integer>");
        iterator++;
    }
    else if (lexem == "'"){
        add_to_syntax_buffer_segment(depthTree, "<empty>", 0);
    }
    else {
        isError = true;
        outputErrorForSyntax("<unsigned-integer>", lexem, output, row, line);
    }

    depthTree--;



}

void complex_number(int &iterator, int &depthTree, ofstream& output) {
    if(isError)
        return;

    add_to_syntax_buffer_segment(depthTree, "<complex-number>", 0);

    depthTree++;

    left_part(iterator, depthTree, output);

    right_part(iterator, depthTree, output);

    depthTree--;

}

void constant(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    add_to_syntax_buffer_segment(depthTree, "<constant>", 0);

    depthTree++;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "'" ) {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("'", lexem, output, row, line);
        return;
    }

    complex_number(iterator, depthTree, output);

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "'" ) {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("'", lexem, output, row, line);
    }

    depthTree--;
}

void constant_declaration(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    depthTree++;

    add_to_syntax_buffer_segment(depthTree, "<constant-declaration>", 0);

    depthTree++;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(ascii >= 1000) {
        add_to_syntax_buffer(depthTree, lexem, ascii, "<constant-identifier>");
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("<constant-identifier>", lexem, output, row, line);
    }

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "=") {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("=", lexem, output, row, line);
    }

    constant(iterator, depthTree, output);

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == ";") {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax(";", lexem, output, row, line);
    }

    depthTree-= 2;
}

void constant_declarations_list(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line = buffer.size();

    if(isError)
        return;
    depthTree++;

    add_to_syntax_buffer_segment(depthTree, "<constant-declarations-list>", 0);

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);

    if(lexem == "BEGIN") {
        depthTree++;
        add_to_syntax_buffer_segment(depthTree, "<empty>", 0);
        depthTree--;
        return;
    }

    constant_declaration(iterator, depthTree, output);

    depthTree--;
}

void const_declarations(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line, len = buffer.size();

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "CONST" && ascii >= 400 && ascii < 500) {
        add_to_syntax_buffer(depthTree, lexem, ascii, "<constant-declarations>");
        iterator++;
    }
    else if(lexem == "BEGIN") {
        add_to_syntax_buffer(depthTree, "<empty>", 0, "<constant-declarations>");
        return;
    }
    else {
        isError = true;
        outputErrorForSyntax("CONST", lexem, output, row, line);
    }

    while ( len >= iterator && !isError && lexem != "BEGIN") {

        constant_declarations_list(iterator, depthTree, output);

        std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    }
    depthTree--;
}

void declarations(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";

    if(isError)
        return;

    add_to_syntax_buffer_segment(depthTree, "<declarations>", 0);

    depthTree++;

    const_declarations(iterator, depthTree, output);

    depthTree--;

}

void statements_list(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "END" && ascii >= 400 && ascii < 500) {
        add_to_syntax_buffer(depthTree, "<empty>", 0, "<statments-list>");
    }
    else {
        isError = true;
        outputErrorForSyntax("<empty>", lexem, output, row, line);
    }
}

void block(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    add_to_syntax_buffer_segment(depthTree, "<block>", 0);
    depthTree++;
    declarations(iterator, depthTree, output);

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "BEGIN" && ascii >= 400 && ascii < 500) {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("BEGIN", lexem, output, row, line);
    }

    statements_list(iterator, depthTree, output);

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == "END" && ascii >= 400 && ascii < 500) {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("END", lexem, output, row, line);
    }

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(lexem == ".") {
        add_to_syntax_buffer_segment(depthTree, lexem, ascii);
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax(".", lexem, output, row, line);
    }
    depthTree--;
}

void procedure_identifier(int &iterator, int &depthTree, ofstream& output) {
    string lexem = "";
    int ascii, row, line;

    if(isError)
        return;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);
    if(ascii >= 1000) {
        add_to_syntax_buffer(depthTree, lexem, ascii, "<procedure-identifier>");
        iterator++;
    }
    else {
        isError = true;
        outputErrorForSyntax("<procedure-identifier>", lexem, output, row, line);
    }
}

void program(int &iterator, ofstream& output) {
    syntaxBuffer.emplace_back("<signal-program>");
    string lexem = "";
    int ascii, row, line, depthTree = 0;

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);

    if(lexem == "PROGRAM" && ascii >= 400 && ascii < 500) {
        add_to_syntax_buffer(depthTree, lexem, ascii, "<program>");
        iterator++;
        depthTree++;
    }
    else {
        isError = true;
        outputErrorForSyntax("PROGRAM", lexem, output, row, line);
    }

    procedure_identifier(iterator, depthTree, output);

    std::tie(lexem, ascii, row, line) = buffer.at(iterator);

    if(!isError) {
        if(lexem == ";") {
            add_to_syntax_buffer_segment(depthTree, lexem, ascii);
            iterator++;
        }
        else {
            isError = true;
            outputErrorForSyntax(";", lexem, output, row, line);
        }
    }

    block(iterator, depthTree, output);
}

void syntaxAnalize (fs::path filePath, fs::path fileOutput) {

    if(isError)
        return;

    const fs::path workdir = fs::current_path();
    const fs::path configPath = workdir.generic_string() + "/tests/" + fileOutput.generic_string() + "/generatedSyntax.txt";
    int iter = 0;
    ofstream output(configPath);

    program(iter, output);

    output_syntax_result(output);

}

int main() {
    const fs::path workdir = fs::current_path();
    const fs::path configPath = workdir / "tests";
    const auto fileName = "test.sig";
    for(fs::directory_iterator it(configPath), end; it != end ; ++it)
    {
        const auto output = it->path().filename();
        const fs::path  result = it->path().generic_string() + "/" + it->path().filename().generic_string() + ".sig";
        ifstream check(result);
        if(check.is_open())
        {
            lexemAnalize(result, output);
            syntaxAnalize(result, output);
            complex.clear();
            identificator.clear();
            buffer.clear();
        }

    }

    return 0;
}
