#include <tuple>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <experimental/filesystem>
#include <iomanip>

namespace fs = std::experimental::filesystem;
using namespace std;

std::vector<std::tuple<std::string, int>> key_words = {
        make_tuple("BEGIN", 401),
        make_tuple("END", 402),
        make_tuple("PROGRAM", 403),
        make_tuple("CONST", 404),
        make_tuple("$EXP", 405)
};
std::vector<std::tuple<std::string, int, int, int>> buffer;
std::vector<std::tuple<std::string, int>> identificator;
std::vector<std::tuple<std::string, int>> complex;

int search_key_words(string key_word) {
    string lexem;
    int code;
    for(auto const& value: key_words)  {
        std::tie(lexem, code) = value;
        if(lexem == key_word)
            return code;
    }
    return 0;
}

int check_complex_words(string word) {
    string lexem;
    int code;
    for(auto const& value: complex)  {
        std::tie(lexem, code) = value;
        if(!lexem.compare(word)) {
            return code;
        }
    }
    return 0;
}

void set_complex(string word, int row, int line, int &count) {
    int code = check_complex_words(word);
    if(!code) {
        buffer.emplace_back(std::make_tuple(word, count, row, line));
        complex.emplace_back(std::make_tuple(word, count++));
    } else {
        buffer.emplace_back(std::make_tuple(word, code, row, line));
    }
}

int check_identifier_words(string word) {
    string lexem;
    int code;
    for(auto const& value: identificator)  {
        std::tie(lexem, code) = value;
        if(!lexem.compare(word)) {
            return code;
        }
    }
    return 0;
}

void set_identifier(string word, int row, int line, int &count) {
    int code = check_identifier_words(word);
    if(!code) {
        buffer.emplace_back(std::make_tuple(word, count, row, line));
        identificator.emplace_back(std::make_tuple(word, count++));
    } else {
        buffer.emplace_back(std::make_tuple(word, code, row, line));
    }
}

void outputError (string error, ofstream& output, int row, int line) {
    output << error << " (row: " << row << " line: " << line << ")" << endl;
}

void outputResult(ofstream& output) {
    string lexem;
    int ascii, row, line;
    for(auto const& value: buffer)  {
        std::tie(lexem, ascii, row, line) = value;
        output << setw(10) << left << lexem <<
               setw(10) << left << ascii <<
               setw(6)<<" line: " << line <<  setw(5) << left<< " row: "<< row << std::endl;
    }
}

void lexemAnalize (fs::path filePath, fs::path fileOutput) {
    string lexem;
    bool isComment = false,
            isComplex = false,
            isBracket = false,
            isError = false,
            isConstant = false;
    int line = 1,
            row = 0,
            row_comment,
            line_comment,
            ascii,
            count_identificator = 1001,
            count_complex = 501;

    const fs::path workdir = fs::current_path();
    const fs::path configPath = workdir.generic_string() + "/tests/" + fileOutput.generic_string() + "/generated.txt";

    ofstream output(configPath);
    if(!output.is_open())
    {
        std::cout << "file didn't open";
    }
    ifstream test(filePath);
    if(!test.is_open())
    {
        std::cout << "file didn't open";
    }
    char symbol{0};

    while( test.get(symbol))
    {
        row++;
        ascii = (int)symbol;

        if(!lexem.compare("(*")) {        // equals beginning comments
            row_comment = row - 1;
            line_comment = line;
            isComment = true;
        }

        if(isBracket && ascii == 42) {
            lexem += symbol;
        }
        else if(isBracket && !isComment && lexem == "(") {
            buffer.emplace_back(std::make_tuple(lexem, 40, row - 1, line));
            lexem = "";
        }


        if(isComment && ascii == 42) {
            lexem = symbol;
        }
        else if(isComment && ascii == 41) {
            if(lexem.length() == 1)
                lexem += symbol;
        }

        if(isComment) {
            if(!lexem.compare("*)")) {
                isComment = false;
                isBracket = false;
            }
            if(symbol != '*')
                lexem = "";
            if(symbol == '\n') {
                row = 0;
                line++;
            }

        }
        else if(ascii == 40) { //(
            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(isConstant) {
                set_complex(lexem, row - lexem.length(), line, count_complex);
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            isConstant = false;
            lexem = symbol;
            isBracket = true;
        }
        else if(ascii >= 48 && ascii <= 57) {
            if(lexem.length() == 0) {
                isConstant = true;
            }
            lexem += symbol;
        }
        else if((ascii >= 65 && ascii <= 90) || (ascii >= 97 && ascii <= 122) || ascii == 36) { // letters
            if(isConstant) {
                outputError("first symbol digit is incorrect", output, row - lexem.length(), line);

                isError = true;
                break;
            }
            lexem += symbol;
        }
        else if(ascii == 32 || ascii == 9) { // space or tab

            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(isConstant) {
                set_complex(lexem, row - lexem.length(), line, count_complex);
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            isConstant = false;
            lexem = "";
        }
        else if(ascii == 59 || ascii == 46 || ascii == 61 || ascii ==44 || ascii == 39 || ascii == 41) {//if symbols equal ; . = , ' $ )
            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(isConstant) {
                set_complex(lexem, row - lexem.length(), line, count_complex);
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            isConstant = false;
            if(ascii == 41) {
                buffer.emplace_back(std::make_tuple(")", ascii, row, line));
                isBracket = false;
            } else {
                string temp;
                temp = symbol;
                buffer.emplace_back(std::make_tuple(temp, ascii, row, line));
            }
            lexem = "";
        }
        else if(symbol == '\n') {
            isConstant = false;
            const int key_word = search_key_words(lexem);
            if(key_word) {
                buffer.emplace_back(std::make_tuple(lexem, key_word, row - lexem.length(), line));
            }
            else if(lexem != "") {
                set_identifier(lexem, row - lexem.length(), line, count_identificator);
            }
            lexem = "";
            line++;
            row = 0;
        }
        else if(ascii != 39 && ascii != 42){
            outputError("not existing symbol", output, row, line);
            isError = true;
            break;
        }

    }
    if(isComment || isBracket) {
        outputError("comment isn't close", output, row_comment, line_comment);
        isError = true;
    }
    if(!isError)
        outputResult(output);


    complex.clear();
    identificator.clear();
    buffer.clear();
    output.close();
}

int main() {
    const fs::path workdir = fs::current_path();
    const fs::path configPath = workdir / "tests";
    const auto fileName = "test.sig";
    for(fs::directory_iterator it(configPath), end; it != end ; ++it)
    {
        const auto output = it->path().filename();
        const fs::path  result = it->path().generic_string() + "/" + it->path().filename().generic_string() + ".sig";
        ifstream check(result);
        if(check.is_open())
        {
            lexemAnalize(result, output);
        }

    }

    return 0;
}
